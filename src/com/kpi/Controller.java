package com.kpi;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;

import static javafx.scene.paint.Color.BLACK;

public class Controller {

    @FXML
    private Pane mainPane;
    @FXML
    private Pane mainPane1;
    @FXML
    private CheckBox degreeCheckbox;
    @FXML
    private TextField centerXfield;
    @FXML
    private TextField centerYfield;
    @FXML
    private TextField RparameterField;
    @FXML
    private TextField mParameterField;
    @FXML
    private TextField HparameterField;
    @FXML
    private TextField NparameterField;
    @FXML
    private TextField lineWidthField;
    @FXML
    private TextField lineColorField;
    @FXML
    private TextField degreeField;
    @FXML
    private TextField centerXfield1;
    @FXML
    private TextField centerYfield1;
    @FXML
    private TextField mainRField;
    @FXML
    private TextField NsmallField;


    @FXML
    public void MouseOrnamentClick(ActionEvent actionEvent){
        double localCenterX = Double.parseDouble(centerXfield1.getCharacters().toString());
        double localCenterY = Double.parseDouble(centerYfield1.getCharacters().toString());
        double localR = Double.parseDouble(mainRField.getCharacters().toString());
        int localN = Integer.parseInt(NsmallField.getCharacters().toString());
        double localDegree = Double.parseDouble(degreeField.getCharacters().toString());
        Ornament myOrnament = new Ornament(localCenterX, localCenterY, localR, localN);
        if(degreeCheckbox.isSelected()){
            if(localDegree == 0.0){
                Group localMyOrnament = myOrnament.generateOrnament();
                localMyOrnament.getTransforms().add(new Rotate(localDegree));
                mainPane1.getChildren().add(localMyOrnament);
            }
            else
            for (int i = 0; i < localDegree; i = i + 5) {
                Group localMyOrnament = myOrnament.generateOrnament();
                localMyOrnament.getTransforms().add(new Rotate(i));
                mainPane1.getChildren().addAll(localMyOrnament);
            }
        }
        else {
            Group localMyOrnament = myOrnament.generateOrnament();
            localMyOrnament.getTransforms().add(new Rotate(localDegree));
            mainPane1.getChildren().add(localMyOrnament);
        }
    }

    @FXML
    public void MainPrimitiveClick(ActionEvent actionEvent) {
        double localCenterX = Double.parseDouble(centerXfield.getCharacters().toString());
        double localCenterY = Double.parseDouble(centerYfield.getCharacters().toString());
        double localR = Double.parseDouble(RparameterField.getCharacters().toString());
        double localm = Double.parseDouble(mParameterField.getCharacters().toString());
        double localH = Double.parseDouble(HparameterField.getCharacters().toString());
        double localN = Double.parseDouble(NparameterField.getCharacters().toString());
        double localWidth = Double.parseDouble(lineWidthField.getCharacters().toString());
        Color localLineColor = Color.web(lineColorField.getCharacters().toString());
        Primitive myPrimitive = new Primitive(localCenterX,localCenterY, localR, localm, localH, localN);
        myPrimitive.setLineWidth(localWidth);
        myPrimitive.setLineColor(localLineColor);
        mainPane.getChildren().add(myPrimitive.makePrimitive());
    }

    public void ClearClick(ActionEvent actionEvent) {
        mainPane.getChildren().clear();
    }

    public void ClearClick2(ActionEvent actionEvent) {
        mainPane1.getChildren().clear();
    }
}
