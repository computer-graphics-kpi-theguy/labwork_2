package com.kpi;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;


public class Primitive {
    private double centerX;
    private double centerY;
    private double R1;
    private double m1;
    private double H1;
    private double N1;
    private Color LineColor = Color.BLACK;
    private double LineWidth = 1.0;
    private double heightCanvas;
    private double widthCanvas;

    public void setHeightCanvas(double heightCanvas) {
        this.heightCanvas = heightCanvas;
    }

    public void setWidthCanvas(double widthCanvas) {
        this.widthCanvas = widthCanvas;
    }

    public void setLineColor(Color lineColor) {
        LineColor = lineColor;
    }

    public void setLineWidth(double lineWidth) {
        LineWidth = lineWidth;
    }

    public Primitive(double centerX, double centerY, double r1, double m1, double h1, double n1) {
        this.centerX = centerX;
        this.centerY = centerY;
        R1 = r1;
        this.m1 = m1;
        H1 = h1;
        N1 = n1;
    }

    public Group makePrimitive() {
        Group primitiveGroup = new Group();
        primitiveGroup.setLayoutX(centerX);
        primitiveGroup.setLayoutY(centerY);
        double dt = Math.PI / 180;
        Path path = new Path();
        primitiveGroup.getChildren().add(path);
        path.setStroke(LineColor);
        path.setStrokeWidth(LineWidth);
        path.getElements().add(new MoveTo((R1 + m1 * R1) + H1, 0));
        for (double t = 0; t < (N1 * Math.PI); t += dt){
            double x = ((R1 + m1 * R1) * Math.cos(m1 * t) + H1 * Math.cos(t + m1 * t));
            double y = ((R1 + m1 * R1) * Math.sin(m1 * t) + H1 * Math.sin(t + m1 * t));
            path.getElements().add(new LineTo(x, y));
        }
        path.getElements().add(new ClosePath());
        return primitiveGroup;
    }
}
